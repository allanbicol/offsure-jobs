<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class PageController extends Controller
{
    function index(){
        return view('index');
    }


    function sendEmail(Request $request)
    {
        $data = [
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'mobile'=>$request->input('mobile')
        ];
        // dump($data);
        // print_r($data['services']);
    // return view('mail.mail1',['data'=>$data]);
    // exit();

    //    Mail::to('bicolallan@gmail.com')->send(new SendMail($data));
        $files = $request->file('myfile');
        $email = Mail::send('mail.mail1', ['data'=>$data], function($message) use ($files) {
            $message->from('support@healthsol.io',"Offsure Global");
            $message->to('careers@offsure.com');
            // $message->to('bicolallan@gmail.com');
            $message->bcc('allan.bicol@offsure.com');
            $message->subject('Appointment Specialists Application');

            $message->attach($files->getRealPath(), array(
                'as' => $files->getClientOriginalName(), // If you want you can chnage original name to custom name
                'mime' => $files->getMimeType())
            );

        });

        return back()->with('success','Message sent successfully!');
    }
}
