<!doctype html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Urgent Hiring Appointment Specialists</title>

    <meta name="description" content="-Outbound Call Rockstar -Fluent in the English, verbal and comprehension -Experience in appointment scheduling and coordination">
    <link rel="canonical" href="{{route('homepage')}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Urgent Hiring Appointment Specialists">
    <meta property="og:description" content="-Outbound Call Rockstar -Fluent in the English, verbal and comprehension -Experience in appointment scheduling and coordination">
    <meta property="og:image" content="{{ URL::asset('assets/images/appointment-specialist.jpg')}}">
    <meta property="og:url" content="{{route('homepage')}}">
    <meta property="og:site_name" content="Offsure">

    <meta name="twitter:title" content="Urgent Hiring Appointment Specialists">
    <meta name="twitter:description" content="-Outbound Call Rockstar -Fluent in the English, verbal and comprehension -Experience in appointment scheduling and coordination">
    <meta name="twitter:image" content="{{ URL::asset('assets/images/appointment-specialist.jpg')}}">
    <meta name="twitter:card" content="summary_large_image">
    <!--favicon-->
    {{-- <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" /> --}}
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
    <!--owl carousel css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/owl.carousel.min.css')}}">
    <!--icofont css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/icofont.min.css')}}">
    <!--magnific popup css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/magnific-popup.css')}}">
    <!--animate css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/animate.css')}}">
    <!--main css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/responsive.css')}}">
</head>

<body>
    <!--start preloader-->
    {{-- <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell align-middle">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div> --}}
    <!--end preloader-->
    <!--start header-->
    {{-- <header id="header">
        <div class="container">
            <nav class="navbar navbar-expand-lg justify-content-center">

                <a class="logo" href="#"><img src="{{ URL::asset('assets/images/logo.png')}}" width="200px" alt="logo"></a>

                <a class="logo-scroll" href="#"><img src="{{ URL::asset('assets/images/logo-scroll.png')}}" width="200px" alt="logo"></a>
                <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
                        <span class="icofont-navigation-menu"></span>
                    </button>
                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
                    <ul class="navbar-nav mx-auto text-center">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="1">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="3">Author</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="4">Reviews</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="6">Contact</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav flex-row justify-content-center flex-nowrap buy-btn">
                        <li class="nav-item"><a class="nav-link" href="#product-area"><i class="icofont-cart-alt"></i> Buy Now</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header> --}}
    <!--end header-->
    <!--start home area-->
    <section  data-scroll-index="0">
        {{-- <div class="container"> --}}
            <div class="row">
                    <div id="home-area" class="col-lg-12">
                        <img width="100%" src="{{ URL::asset('assets/images/AppointmentSpecialist-04.jpg')}}">
                    </div>
                <!--start caption content-->
                {{-- <div class="col-lg-6 col-md-7">
                    <div class="caption-content d-table">
                        <div class="d-table-cell align-middle">
                            <h1>Know Your Value: How to Price Your Work?</h1>
                            <p>Have you told yourself a million times that you’re going to launch your business but still find yourself in the same job you were in five, ten or fifteen years ago?</p>
                            <a href="#product-area"><i class="icofont-cart"></i> Order Now</a>
                        </div>
                    </div>
                </div> --}}
                <!--end caption content-->
                <!--start caption image-->
                {{-- <div class="col-lg-6 col-md-5">
                    <div class="caption-img">
                        <img src="{{ URL::asset('assets/images/book.png')}}" width="80%" class="img-fluid animation-jump" alt="">
                    </div>
                </div> --}}
                <!--end caption image-->
            </div>
        {{-- </div> --}}
    </section>
    <section style="background-color:#fff;">
            <div class="container">
                    <div class="row" style="text-align:center;padding-top:30px;">
                        <!--start about content-->
                        <div class="col-lg-12">
                            <h1 style="color:#FF4813;">Appointment Specialists</h1>
                        </div>
                    </div>
            </div>
    </section>
    <!--end home area-->
    <!--start about area-->
    <section id="about-area" data-scroll-index="1" style="background-color:#fff;">
        <div class="container">
            <div class="row" style="padding: 0 30px;">

                <div id="fade"></div>
                <div id="modal" style="text-align:center;">
                    <img id="loader" src="{{ URL::asset('assets/images/mail.gif')}}" />
                    <h2 style="color:#FF4813">Sending Email</h2>
                </div>

                <!--start about image-->
                <div class="col-lg-7">
                        <div class="contact-box">
                                <!--start contact form-->
                                <div class="contact-form">
                                    @if($message = Session::get('success'))
                                    <div class="col-md-12 text-center wow animated fadeInUp">
                                        <div class="alert alert-success alert-block">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    </div>
                                    @endif
                                    <form action="{{route('send-mail')}}" method="post" id="emailForm" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
                                            </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <div class="upload-btn-wrapper">
                                                <a><i class="icofont-attachment" style="font-size:25px;cursor:pointer;color:#FF4813"></i>Upload CV</a>
                                                <input type="file" name="myfile"  required/>
                                            </div>
                                        </div>
                                        <button type="submit" id="btnSubmit">Submit</button>
                                        <div class="messages"></div>
                                    </form>
                                </div>
                                <!--end contact form-->
                            </div>
                    </div>
                    <!--end about image-->
                <!--start about content-->
                <div class="col-lg-5">
                    <div class="about-content">
                        <h4>Qualifications:</h4>
                        <!-- <h2>Best and Fashionable Smart Watch For Easy Life.</h2> -->
                        <ul>
                            <li><i class="icofont-check-circled" style="color:#FF4813"></i> Outbound Call Rockstar</li>
                            <li><i class="icofont-check-circled" style="color:#FF4813"></i> Fluent in the English, verbal and comprehension</li>
                            <li><i class="icofont-check-circled" style="color:#FF4813"></i> Experience in appointment scheduling and coordination</li>
                        </ul>
                        <br>
                        <p>Expect a call or an email from us for your interview 1 day after your submission.</p>

                    </div>
                </div>
                <!--end about content-->

            </div>
        </div>
    </section>
    <!--end about area-->




    <!--start footer-->
    <footer id="footer">
        <div class="container">
            <div class="footer-social-icons text-center">
                <h4>Follow Us</h4>
                <ul>
                    <li><a href=""><i class="icofont-facebook"></i></a></li>
                    <li><a href=""><i class="icofont-twitter"></i></a></li>
                    <li><a href=""><i class="icofont-linkedin"></i></a></li>
                </ul>
            </div>
            <p>Copy &copy; 2019. All Rights Reserved | Offsure Global | Powered  by <a href="http://huntcreative.co.nz" target="_blank">huntcreative</a></p>
        </div>
    </footer>
    <!--end footer-->
    <!--jQuery js-->
    <script src="{{ URL::asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <!--proper js-->
    <script src="{{ URL::asset('assets/js/popper.min.js')}}"></script>
    <!--bootstrap js-->
    <script src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <!--owl carousel js-->
    <script src="{{ URL::asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--magnific popUp js-->
    <script src="{{ URL::asset('assets/js/magnific-popup.min.js')}}"></script>
    <!--scrollIt js-->
    <script src="{{ URL::asset('assets/js/scrollIt.min.js')}}"></script>
    <!--contact js-->
    <script src="{{ URL::asset('assets/js/contact.js')}}"></script>
    <!--validator js-->
    <script src="{{ URL::asset('assets/js/validator.min.js')}}"></script>
    <!--main js-->
    <script src="{{ URL::asset('assets/js/custom.js')}}"></script>
</body>

</html>
