<!doctype html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>Mith Froilan - Know Your Value: How to Price Your Work?</title>
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
    <!--owl carousel css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/owl.carousel.min.css')}}">
    <!--icofont css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/icofont.min.css')}}">
    <!--magnific popup css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/magnific-popup.css')}}">
    <!--animate css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/animate.css')}}">
    <!--main css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/responsive.css')}}">
</head>

<body>
    <!--start preloader-->
    <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell align-middle">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>
    <!--end preloader-->
    <!--start header-->
    <header id="header">
        <div class="container">
            <nav class="navbar navbar-expand-lg justify-content-center">
                <!--logo-->
                <a class="logo" href="#"><img src="{{ URL::asset('assets/images/logo.png')}}" width="200px" alt="logo"></a>
                <!--scroll logo-->
                <a class="logo-scroll" href="#"><img src="{{ URL::asset('assets/images/logo-scroll.png')}}" width="200px" alt="logo"></a>
                <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
                        <span class="icofont-navigation-menu"></span>
                    </button>
                <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
                    <ul class="navbar-nav mx-auto text-center">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="1">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="3">Author</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="4">Reviews</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="6">Contact</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav flex-row justify-content-center flex-nowrap buy-btn">
                        <li class="nav-item"><a class="nav-link" href="#product-area"><i class="icofont-cart-alt"></i> Buy Now</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!--end header-->
    <!--start home area-->
    <section id="home-area" data-scroll-index="0">
        <div class="container">
            <div class="row">
                <!--start caption content-->
                <div class="col-lg-6 col-md-7">
                    <div class="caption-content d-table">
                        <div class="d-table-cell align-middle">
                            <h1>Know Your Value: How to Price Your Work?</h1>
                            <p>Have you told yourself a million times that you’re going to launch your business but still find yourself in the same job you were in five, ten or fifteen years ago?</p>
                            <a href="#product-area"><i class="icofont-cart"></i> Order Now</a>
                        </div>
                    </div>
                </div>
                <!--end caption content-->
                <!--start caption image-->
                <div class="col-lg-6 col-md-5">
                    <div class="caption-img">
                        <img src="{{ URL::asset('assets/images/book.png')}}" width="80%" class="img-fluid animation-jump" alt="">
                    </div>
                </div>
                <!--end caption image-->
            </div>
        </div>
    </section>
    <!--end home area-->
    <!--start about area-->
    <section id="about-area" data-scroll-index="1">
        <div class="container">
            <div class="row">
                <!--start about content-->
                <div class="col-lg-6">
                    <div class="about-content" style="padding-top:50px">
                        <h4>About The Book</h4>
                        <!-- <h2>Best and Fashionable Smart Watch For Easy Life.</h2> -->
                        <p>Have you told yourself a million times that you’re going to launch your business but still find yourself in the same job you were in five, ten or fifteen years ago?</p>
                        <p>Do you daydream of the things you’ve always wanted to do but haven’t found the courage to make it come true? Have you been storming the heavens to take you to where you want to be? It is time to arm your faith with action.</p>
                        <p>Now is the time to make it happen.</p>
                        <p>This book is packed with inspiration and valuable tools to help you:</p>
                        <ul>
                            <li><i class="icofont-check-circled"></i> Discover the truth about your true value and what you are really made of</li>
                            <li><i class="icofont-check-circled"></i> Unleash your talents, skills, and abilities</li>
                            <li><i class="icofont-check-circled"></i> Bring out the best version of yourself</li>
                            <li><i class="icofont-check-circled"></i> Serve the world with your price—passion, resilience, intellect, contribution, and experience</li>
                            <li><i class="icofont-check-circled"></i> Know how to price your work</li>
                            <li><i class="icofont-check-circled"></i> Design the life that God designed just for you</li>
                        </ul>
                        <br>
                        <p>You can make your dream happen. Read this book and start living the life you’ve always wanted to live.</p>

                    </div>
                </div>
                <!--end about content-->
                <!--start about image-->
                <div class="col-lg-6">
                    <div class="about-img">
                        <img src="{{ URL::asset('assets/images/about-img.jpg')}}" class="img-fluid" alt="">
                    </div>
                </div>
                <!--end about image-->
            </div>
        </div>
    </section>
    <!--end about area-->

    <!--start video area-->
    <section id="video-area">
        <div class="video-overlay"></div>
        <div class="container">
            <div class="row">
                <!--start video content-->
                <div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1">
                    <div class="video-content text-center">
                        <h2>Got a Dream? It's Time to Make It Happen.</h2>
                       <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, rerum aut! Tempore maiores ipsum sequi porro ratione officia.</p> -->
                       <!--  <div class="video-player">
                            <div class="pulse1"></div>
                            <div class="pulse2"></div>
                            <a class="popup-video" href="https://www.youtube.com/watch?v=EOA1oXpLT0w"><i class="icofont-play"></i></a>
                        </div> -->
                    </div>
                </div>
                <!--end video content-->
            </div>
        </div>
    </section>
    <!--end video area-->

    <!--start why choose area-->
    <section id="author" data-scroll-index="3">
        <div class="container" style="padding-top:140px;">
            <div class="row">
                <!--start heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>Mith Froilan</h2>
                        <h4>Writer and Entrepreneur</h4>
                    </div>
                </div>
                <!--end heading-->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="why-choose-img text-center">
                        <img src="{{ URL::asset('assets/images/author1.png')}}" class="img-fluid animation-jump" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <!--start why choose single-->
                    <div class="why-choose-single">

                        <div class="content">
                            <p>Mith Froilan is a technical writer and an entrepreneur. She is a proud native of Laoang in Northern Samar, Philippines. She received her Bachelor of Business Administration major in Management degree from the University of the Philippines-Tacloban College.
							</p>
							<p>After spending almost 10 years in the corporate world creating business plans for several local and overseas companies, she decided to kick start her own company—MFroilan Training and Consultancy—which provides training manuals and assessment tools to some training schools and colleges in Australia.</p>
							<p>She launched Make It Happen (MITH) Scholarship Program to help financially challenged but deserving students from her hometown in Samar to finish their university education. She hopes to register the Make It Happen (MITH) Scholarship Foundation by 2018.</p>
							<p>Above anything else, she is a proud sister to Michael, Titing, Tacs, Tonton, Kevin, Kenneth, and Angela, and a loving daughter to Papa Boy and Nanay Sayong.</p>
                        </div>
                    </div>
                    <!--end why choose single-->

                </div>
            </div>
        </div>
    </section>
    <!--end why choose area-->
    <!--start newsletter area-->
    <section id="newsletter-area">
        <div class="newsletter-overlay"></div>
        <div class="container">
            <div class="newsletter-box">
                <div class="row">
                    <div class="col-lg-6">
                        <h2>Subscribe Our Newsletter</h2>
                        <p>We will send you latest update of the product.</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="newsletter-form">
                            <form action="#" method="post">
                                <input type="email" class="form-control" placeholder="Enter Your Email">
                                <button type="submit">Submit</button>
                            </form>
                            <div id="response"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end newsletter area-->
    <!--start product area-->
    <section id="product-area" data-scroll-index="3">
        <div class="container" style="padding-top:40px">
            <div class="row">
                <!--start heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>Choose Best One</h2>

                    </div>
                </div>
                <!--end heading-->
            </div>
            <div class="row">
                <!--start product single-->
                <div class="col-lg-3 col-md-6">
                    <!-- <div class="product-single text-center">
                        <h4>Casual</h4>
                        <img src="assets/images/watch-4.png" class="img-fluid" alt="">
                        <h2>$199</h2>
                        <a href=""><i class="icofont-cart-alt"></i> Buy Now</a>
                    </div> -->
                </div>
                <!--end product single-->
                <!--start product single-->
                <div class="col-lg-3 col-md-6">
                    <div class="product-single text-center">
                        <h4>eBook</h4>
                        <img src="{{ URL::asset('assets/images/eBook.png')}}" class="img-fluid" alt="">
                        <h2>USD 2.99</h2>
                        <a href="https://payhip.com/b/U8mC" target="_blank"><i class="icofont-cart-alt"></i> Buy Now</a>
                    </div>
                </div>
                <!--end product single-->
                <!--start product single-->
                <div class="col-lg-3 col-md-6">
                    <div class="product-single text-center">
                        <h4>Paperback</h4>
                        <img src="{{ URL::asset('assets/images/book.png')}}" style="max-width:235px;" class="img-fluid" alt="">
                        <h2>PHP 350.00</h2>
                        <a href=""><i class="icofont-cart-alt"></i> Buy Now</a>
                    </div>
                </div>
                <!--end product single-->
                <!--start product single-->
                <div class="col-lg-3 col-md-6">
                   <!--  <div class="product-single text-center">
                        <h4>Extra</h4>
                        <img src="assets/images/watch-7.png" class="img-fluid" alt="">
                        <h2>$220</h2>
                        <a href=""><i class="icofont-cart-alt"></i> Buy Now</a>
                    </div> -->
                </div>
                <!--end product single-->
            </div>
        </div>
    </section>
    <!--staendrt product area-->
    <!--start testimonial area-->
    <section id="testimonial-area" data-scroll-index="4">
        <div class="container" style="padding-top:50px;">
            <div class="row">
                <!--start heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>Reviews</h2>
                        <p>What the readers said about the book.</p>
                    </div>
                </div>
                <!--end heading-->
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="testimonial-carousel owl-carousel">
                        <!--start testimonial single-->
                        <div class="testimonial-single text-center">
                            <div class="client-info">
                                <img src="{{ URL::asset('assets/images/reviewers/cheryl.png')}}" class="img-fluid" alt="">
                                <h4>Cheryl Jansen</h4>
                                <p>Hong Kong</p>
                            </div>
                            <div class="client-comment">
                                <p>This book provides its readers hope and a practical aspiration of pursuing one’s own business and bringing out the best in one’s chosen career, without compromising the vital truths of faith and generosity. It is profuse with the author’s personal life experiences, while she injects spirituality and relationship with the Lord and connects it to an insight to the chapter topic.</p>
                                <p><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i></p>
                            </div>
                        </div>
                        <!--end testimonial single-->
                        <!--start testimonial single-->
                        <div class="testimonial-single text-center">
                            <div class="client-info">
                                <img src="{{ URL::asset('assets/images/reviewers/louela.png')}}" class="img-fluid" alt="">
                                <h4>Louela Calagui</h4>
                                <p>Singapore</p>
                            </div>
                            <div class="client-comment">
                                <p>The book is an inspirational read that encourages readers in being aware of their value as a person, that they are the Lord’s masterpiece and are exceptional, which could be a guide in pricing one’s work. It is so common for us, Filipinos to be shy and sell ourselves short, but when we know our worth, we wouldn’t be reluctant pricing ourselves.</p>
                                <p><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i></p>
                            </div>
                        </div>
                        <!--end testimonial single-->
                        <!--start testimonial single-->
                        <div class="testimonial-single text-center">
                            <div class="client-info">
                                <img src="{{ URL::asset('assets/images/reviewers/ghia.png')}}" class="img-fluid" alt="">
                                <h4>Ghia Marie Ocon-Espiritu</h4>
                                <p>Makati, Philippines</p>
                            </div>
                            <div class="client-comment">
                                <p>We know that everything is a process, but this is a great book that can surely motivate anyone to kick start one’s journey in establishing a business and being relentless to one’s dreams because you know your value.</p>
                                <p><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i></p>
                            </div>
                        </div>
                        <!--end testimonial single-->
                        <!--start testimonial single-->
                        <div class="testimonial-single text-center">
                            <div class="client-info">
                                <img src="{{ URL::asset('assets/images/reviewers/ghia.png')}}" class="img-fluid" alt="">
                                <h4>Athena Reiss</h4>
                                <p>Buderim, Queensland, Australia</p>
                            </div>
                            <div class="client-comment">
                                <p>This is a clever book. It is very informative, possesses doable strategies and is easy to comprehend worth my time reading it.
                                    Being a general practitioner in the field of Dentistry in Philippines and now working in the tree lopping business owned by my husband in Australia is ok but i want to accomplish more.
                                    I want to establish my own business too
                                    I get frustrated for not being able to start my own
                                    After so many talents and skills i possess.
                                    Yesterday, after reading the book, i was so inspired to start my own business again.
                                    With  the help of the author, Mith Froilan i registered an account in Stock Trading Business and I'm excited to start trading already!</p>
                                <p><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i></p>
                            </div>
                        </div>
                        <!--end testimonial single-->

                        <!--start testimonial single-->
                        <div class="testimonial-single text-center">
                            <div class="client-info">
                                <img src="{{ URL::asset('assets/images/reviewers/pauline.png')}}" class="img-fluid" alt="">
                                <h4>Pauline Beatrice Balitaan</h4>
                                <p>Manila, Philippines</p>
                            </div>
                            <div class="client-comment">
                                <p>Incredible little book! It astonishes me that while I was reading, I can imagine the author, speaking in front of me, but in a formal and organized manner. Here, she summed up her thoughts, values and dreams. It has a very natural flow which makes it hard to put down and so easy to read. She was speaking her heart so genuinely!</p>
                                <p><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i><i class="icofont-star"></i></p>
                            </div>
                        </div>
                        <!--end testimonial single-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end testimonial area-->

    <!--start contact area-->
    <section id="contact-area" data-scroll-index="6">
        <div class="container" style="padding-top:50px;">
            <div class="row">
                <!--start heading-->
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="section-heading text-center">
                        <h2>Contact Author</h2>
                        <p>Say hi or ask something.</p>
                    </div>
                </div>
                <!--end heading-->
            </div>
            <div class="row">
                <!--start contact info-->
                <div class="col-lg-4 col-md-5">
                    <div class="contact-info">
                        <!--start contact info single-->
                        <div class="contact-info-single">
                            <div class="icon">
                                <i class="icofont-phone"></i>
                            </div>
                            <div class="content">
                                <p><br>+639087491763</p>
                            </div>
                        </div>
                        <!--end contact info single-->
                        <!--start contact info single-->
                        <div class="contact-info-single">
                            <div class="icon">
                                <i class="icofont-email"></i>
                            </div>
                            <div class="content">
                                <p>mith@mfroilan.com <br> admin@mfroilan.com</p>
                            </div>
                        </div>
                        <!--end contact info single-->
                        <!--start contact info single-->
                        <div class="contact-info-single">
                            <div class="icon">
                                <i class="icofont-google-map"></i>
                            </div>
                            <div class="content">
                                <p>Northern Samar<br>Philippines</p>
                            </div>
                        </div>
                        <!--end contact info single-->
                    </div>
                </div>
                <!--end contact info-->
                <!--start contact box-->
                <div class="col-lg-8 col-md-7">
                    <div class="contact-box">
                        <!--start contact form-->
                        <div class="contact-form">
                            @if($message = Session::get('success'))
                            <div class="col-md-12 text-center wow animated fadeInUp">
                                <div class="alert alert-success alert-block">
                                    <strong>{{ $message }}</strong>
                                </div>
                            </div>
                            @endif
                            <form action="{{route('send-mail')}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name*" required="required" data-error="Name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email*" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="message" name="message" rows="10" placeholder="Write Your Message*" required="required" data-error="Please, leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button type="submit">Submit</button>
                                <div class="messages"></div>
                            </form>
                        </div>
                        <!--end contact form-->
                    </div>
                </div>
                <!--end contact box-->
            </div>
        </div>
    </section>
    <!--end contact area-->
    <!--start footer-->
    <footer id="footer">
        <div class="container">
            <div class="footer-social-icons text-center">
                <h4>Follow Us</h4>
                <ul>
                    <li><a href=""><i class="icofont-facebook"></i></a></li>
                    <li><a href=""><i class="icofont-twitter"></i></a></li>
                    <li><a href=""><i class="icofont-linkedin"></i></a></li>
                </ul>
            </div>
            <p>Copy &copy; 2018. All Rights Reserved | Mith Froilan | Powered  by <a href="http://theabcreative.com" target="_blank">ABCreative</a></p>
        </div>
    </footer>
    <!--end footer-->
    <!--jQuery js-->
    <script src="{{ URL::asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <!--proper js-->
    <script src="{{ URL::asset('assets/js/popper.min.js')}}"></script>
    <!--bootstrap js-->
    <script src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <!--owl carousel js-->
    <script src="{{ URL::asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--magnific popUp js-->
    <script src="{{ URL::asset('assets/js/magnific-popup.min.js')}}"></script>
    <!--scrollIt js-->
    <script src="{{ URL::asset('assets/js/scrollIt.min.js')}}"></script>
    <!--contact js-->
    <script src="{{ URL::asset('assets/js/contact.js')}}"></script>
    <!--validator js-->
    <script src="{{ URL::asset('assets/js/validator.min.js')}}"></script>
    <!--main js-->
    <script src="{{ URL::asset('assets/js/custom.js')}}"></script>
</body>

</html>
